'use strict';

// https://medium.com/@fsufitch/is-javascript-array-sort-stable-46b90822543f
Array.prototype.stableSort = function(cmp) {
    cmp = !!cmp ? cmp : (a, b) => {
        if (a < b) return -1;
        if (a > b) return 1;
        return 0;
    };
    let stabilizedThis = this.map((el, index) => [el, index]);
    let stableCmp = (a, b) => {
        let order = cmp(a[0], b[0]);
        if (order != 0) return order;
        return a[1] - b[1];
    }
    stabilizedThis.sort(stableCmp);
    for (let i=0; i<this.length; i++) {
        this[i] = stabilizedThis[i][0];
    }
    return this;
}

class Node {
    constructor(id, port, inputLeft = undefined, inputRight = undefined){
        this.id = id;
        this.port = port;
        this.inputLeft = inputLeft;
        this.inputRight = inputRight;
        this.level = -1;
        this.col = -1;
        this.outEdges = [];
    }
}
var Port = {
    SRC: -1,
    ID:   0,
    AND:  1,
    OR:   2
};

class Graph {
    constructor(graphChar, inputIds, outputIds){
        this.nodes = clone(graphChar);
        // Add dummy nodes for output
        for (var i = 0; i < outputIds.length; i++)
            this.nodes[outputIds[i]+" dummy"] = outputIds[i];
        outputIds = outputIds.map(x => x+" dummy");
        Graph.objectifyNodes(this.nodes);
        this.inputs = inputIds.map(node => this.nodes[node]);
        this.outputs = outputIds.map(node => this.nodes[node]);
        this.orderedNodes = this.toposort();
        var lastLevel = -1, lastCol = -1;
        this.levels = [];
        var dummyNodes = [], dummyNodesTmp = [];
        var i = -1, j = -1;
        while (1){
            var node = this.orderedNodes[++i]
            if (node === undefined){
                node = dummyNodes[++j];
                i--;
            }
            if (node === undefined)
                break;
            if (node.level == -1)
                continue; // Unreachable node
            if (node.level != lastLevel){
                dummyNodes = dummyNodesTmp;
                dummyNodesTmp = [];
                this.levels.push([]);
                lastLevel = node.level;
                lastCol = 0;
                j = -1;
            }
            this.levels[this.levels.length-1].push(node);
            node.col = lastCol++;
            if (node.port == Port.ID && node.outEdges.length == 0){ // Add dummy nodes to level it
                var tmp = new Node(node.id, Port.ID);
                tmp.level = node.level+1;
                tmp.col = node.col;
                dummyNodesTmp.push(tmp);
                node.outEdges.push(tmp);
            }
        }
        this.reorder(this.levels[0], inputIds);
        this.reorder(this.levels[this.levels.length-1], outputIds);
    }
    reorder(vec, ord){
        var ordered = {};
        for (var i = 0; i < ord.length; i++)
            ordered[ord[i]] = i;
        for (var i = 0; i < vec.length; i++)
            vec[i].col = ordered[vec[i].id];
        vec.sort((a,b) => a.col-b.col);
    }
    toposort(){
        for (var i = 0; i < this.inputs.length; i++)
            this.inputs[i].level = 0;
        for (var i = 0; i < this.outputs.length; i++)
            this.toposortRec(this.outputs[i]);
        var orderedNodes = [];
        for (var k in this.nodes)
            if (this.nodes.hasOwnProperty(k))
                orderedNodes.push(this.nodes[k]);
        orderedNodes.stableSort((a,b) => a.level - b.level);
        return orderedNodes;
    }
    toposortRec(node){
        if (node.level != -1) return node.level;
        if (node.inputLeft !== undefined){
            node.inputLeft.outEdges.push(node);
            node.level = Math.max(node.level, this.toposortRec(node.inputLeft)+1);
        }
        if (node.inputRight !== undefined){
            node.inputRight.outEdges.push(node);
            node.level = Math.max(node.level, this.toposortRec(node.inputRight)+1);
        }
        return node.level;
    }
    static objectifyNodes(nodes){
        for (var k in nodes)
            if (nodes.hasOwnProperty(k))
                if (typeof(nodes[k]) == "string")
                    nodes[k] = new Node(k, Port.ID, nodes[k]);
                else {
                    if (nodes[k].length == 0)
                        nodes[k] = new Node(k, Port.SRC)
                    else
                        nodes[k] = new Node(k, nodes[k][0], nodes[k][1], nodes[k][2])
                }
        for (var k in nodes)
            if (nodes.hasOwnProperty(k)){
                if (nodes[k].inputLeft !== undefined)
                    nodes[k].inputLeft = nodes[nodes[k].inputLeft]
                if (nodes[k].inputRight !== undefined)
                    nodes[k].inputRight = nodes[nodes[k].inputRight]
            }
    }

    compute(x, t){
        for (var i = 0; i < t; i++)
            x = this.run(x);
        return x;
    }
    run(x){
        var values = {};
        for (var i = 0; i < this.inputs.length; i++)
            values[this.inputs[i].id] = x[i];
        for (var i = 0; i < this.orderedNodes.length; i++){
            var node = this.orderedNodes[i];
            switch (node.port){
                case Port.SRC:
                    break;
                case Port.ID:
                    values[node.id] = values[node.inputLeft.id];
                    break;
                case Port.OR:
                    values[node.id] = values[node.inputLeft.id] | values[node.inputRight.id];
                    break;
                case Port.OR:
                    values[node.id] = values[node.inputLeft.id] & values[node.inputRight.id];
                    break;
            }
        }
        var out = [];
        for (var i = 0; i < this.outputs.length; i++)
            out.push(values[this.outputs[i].id]);
        return out;
    }
}
var PSPACE = (function() {
    var Model = {
        NopeShift: [
            [' ','RU','LD'],
            [' ',' ','DU'],
            [' ','DR','LU']
        ],
        RedShiftOriginal: [
            [' ',' ','DU',' ',' '],
            ['DR','LR','LURU','LR','LD'],
            [['P',0],' ',' ',' ','DU'],
            ['UR','LR','X','LR','LU'],
            [' ',' ','DU',' ',' ']
        ],
        RedShift: [
            ['DR','LURU','LD'],
            [['P',0],' ','DU'],
            ['UR','X','LU']
        ],
        BlueShiftOriginal: [
            [' ',' ','DU',' ',' '],
            ['DR','LR','LURU','LR','LD'],
            ['DU',' ',' ',' ',['P',1]],
            ['UR','LR','X','LR','LU'],
            [' ',' ','DU',' ',' ']
        ],
        BlueShift: [
            ['DR','LURU','LD'],
            ['DU',' ',['P',1]],
            ['UR','X','LU']
        ],
        AbsorptionRedOriginal: [
            [' ',' ','DU',' ',' ',' ',' '],
            ['DR','LR','LURU','LR','LD',' ',' '],
            ['DU',' ',' ',' ','DU',' ',' '],
            ['UR','LR','X','LR','LURU','LR','LD'],
            [' ',' ','DU',' ',' ',' ',['P',0]],
            [' ',' ','DU',' ',' ',' ','DU']
        ],
        AbsorptionRed: [
            ['DR','LURU','LD',' '],
            ['UR','X','LURU','LD'],
            [' ','DU',' ',['P',0]],
        ],
        AbsorptionBlueOriginal: [
            [' ',' ',' ',' ','DU',' ',' '],
            [' ',' ','DR','LR','LURU','LR','LD'],
            [' ',' ','DU',' ',' ',' ','DU'],
            ['DR','LR','URLU','LR','X','LR','LU'],
            [['P',1],' ',' ',' ','DU',' ',' '],
            ['DU',' ',' ',' ','DU',' ',' ']
        ],
        AbsorptionBlue: [
            [' ','DR','LURU','LD'],
            ['DR','ULUR','X','LU'],
            [['P',1],' ','DU',' '],
        ],
        CrossoverOriginal: [
            [' ','DU',' ','DU',' '],
            [' ','UR','LR','DULR','LD'],
            ['DR','LR','LR','LU','DU'],
            ['UR','LD',' ','DR','LU'],
            [' ','DU',' ','DU',' ']
        ],
        Crossover: [
            [' ','UR','LR','DULR','LD'],
            ['DR','LR','LR','LU','DU'],
            ['UR','LD',' ','DR','LU'],
        ],
        BlendOriginal: [
            [' ',' ',' ','DU',' ',' ',' '],
            ['LR','X','LR','LURU','LR','X','LR'],
            [' ','DU',' ',' ',' ','DU',' ']
        ],
        Blend: [
            ['X','LURU','X']
        ],
        IdDelayed: undefined,
        Fanout: undefined,
        OR: undefined,
        AND: undefined,
        StartTrue: undefined,
        StartFalse: undefined,
        
        _leftDelay: [
            ['DR','LU'],
            ['UR','LD']
        ],
        _rightDelay: [
            ['RU','DL'],
            ['RD','UL']
        ]
    };
    Model.Fanout = (function(){
        var board = [];
        var boardWidth = Model.Crossover[0].length+2+2;
        var getRow = (blockChar) => builderGetRow(boardWidth, blockChar);
        var copy = (r, c, model) => builderCopy(board, r, c, model);
        // var upPart = [
        //     [' ','DU',' '],
        //     ['RD','ULUR','LD'],
        //     ['UD',' ','UD']
        // ];
        // var downPart = [
        //     ['UD',' ','UD'],
        //     ['RU','X','LU'],
        //     [' ','DU',' ']
        // ];
        var upPart = [
            ['RD','ULUR','LD']
        ];
        var downPart = [
            ['RU','X','LU']
        ];
        var workingLine = 0;
        for (var i = 0; i < upPart.length+Model.Crossover.length+downPart.length+1; i++) board.push(getRow(' '));

        copy(0,1,upPart); 
        copy(0,boardWidth-4,upPart);

        workingLine += upPart.length;
        copy(workingLine,2,Model.Crossover);
        for (var i = 0; i < 2; i++){
            copy(workingLine+2*i,0,Model._leftDelay);
            copy(workingLine+2*i,boardWidth-2,Model._rightDelay);
        }

        workingLine += Model.Crossover.length;
        copy(workingLine,1,downPart); 
        copy(workingLine,boardWidth-4,downPart);
        board[workingLine][1] = board[workingLine][boardWidth-2] = "LR";

        workingLine += downPart.length;
        for (var i = 3; i < boardWidth-3; i++)
            board[workingLine][i] = "LR";
        board[workingLine][2] = "UR";
        board[workingLine][boardWidth-3] = "UL";
        board[workingLine][Math.floor(boardWidth/2)] = 'X';
        // board[workingLine+1][Math.floor(boardWidth/2)] = 'UD';
        return board;
    })();
    Model.OR = (function(){
        var board = [];
        var boardWidth = Model.Blend[0].length+4;
        var getRow = (blockChar) => builderGetRow(boardWidth, blockChar);
        var copy = (r, c, model) => builderCopy(board, r, c, model);

        // board.push(getRow()); board[0][2] = board[0][4] = "DU";
        
        for (var i = 0; i < Model.BlueShift.length; i++)
            board.unshift(getRow());
        copy(0,3,Model.NopeShift);
        copy(0,1,Model.BlueShift);
        
        board.unshift(getRow());
        copy(0, 2, Model.Blend);
        board[0][1] = "UR";
        
        for (var i = 0; i < Model.RedShift.length; i++)
            board.unshift(getRow());
        copy(0,2,Model.RedShift);
        copy(1,0,Model._leftDelay);
        board[0][1] = "UD";

        for (var i = 0; i < Model.Crossover.length; i++)
            board.unshift(getRow());
        copy(0,0,Model.Crossover);

        for (var i = 0; i < Model.AbsorptionRed.length; i++)
            board.unshift(getRow());
        copy(0,0,Model.AbsorptionRed);
        
        for (var i = 0; i < 5; i++)
            copy(2*i,boardWidth-2,Model._rightDelay);
        board[9][boardWidth-2] = "LR";
        
        for (var i = 0; i < Model.AbsorptionRed.length; i++)
            board.unshift(getRow());
        copy(0,2,Model.AbsorptionRed);
        board[2][1] = "DR";
        board[2][2] = "LR";
        board[2][3] = "LU";
        
        for (var i = 0; i < Model.NopeShift.length; i++)
            board.unshift(getRow());
        copy(0,2,Model.NopeShift);
        // board.unshift(getRow()); board[0][3] = "#";

        return board;
    })();
    Model.AND = (function(){
        var board = [];
        var boardWidth = Model.Blend[0].length+4;
        var getRow = (blockChar) => builderGetRow(boardWidth, blockChar);
        var copy = (r, c, model) => builderCopy(board, r, c, model);

        // board.push(getRow()); board[0][2] = board[0][4] = "DU";
        
        for (var i = 0; i < Model.BlueShift.length; i++)
            board.unshift(getRow());
        copy(0,3,Model.NopeShift);
        copy(0,1,Model.BlueShift);

        board.unshift(getRow());
        copy(0, 2, Model.Blend);
        board[0][boardWidth-2] = "UL";
        
        for (var i = 0; i < Model.BlueShift.length; i++)
            board.unshift(getRow());
        copy(0,2,Model.BlueShift);
        copy(1,boardWidth-2,Model._rightDelay);
        board[0][boardWidth-2] = "UD";

        for (var i = 0; i < Model.Crossover.length; i++)
            board.unshift(getRow());
        copy(0,boardWidth-Model.Crossover[0].length,Model.Crossover);

        for (var i = 0; i < Model.AbsorptionBlue.length; i++)
            board.unshift(getRow());
        copy(0,boardWidth-Model.AbsorptionBlue[0].length,Model.AbsorptionBlue);
        
        for (var i = 0; i < 5; i++)
            copy(2*i,0,Model._leftDelay);
        board[9][1] = "LR";
        
        for (var i = 0; i < Model.AbsorptionBlue.length; i++)
            board.unshift(getRow());
        copy(0,boardWidth-Model.AbsorptionBlue[0].length-2,Model.AbsorptionBlue);
        board[2][boardWidth-2] = "DL";
        board[2][boardWidth-3] = "LR";
        board[2][boardWidth-4] = "RU";
        
        for (var i = 0; i < Model.RedShift.length; i++)
            board.unshift(getRow());
        copy(0,2,Model.RedShift);
        // board.unshift(getRow()); board[0][3] = "#";

        return board;
    })();
    Model.StartTrue = (function(){
        var board = [];
        var boardWidth = Model.RedShift[0].length+1;
        var getRow = (blockChar) => builderGetRow(boardWidth, blockChar);
        var copy = (r, c, model) => builderCopy(board, r, c, model);
        
        board.unshift(getRow());
        board[0][0] = 'DU';
        board[0][2] = ['S',0];

        for (var i = 0; i < Model.RedShift.length; i++){
            board.unshift(getRow());
            board[0][0] = 'DU';
        }
        copy(0,1,Model.RedShift);

        board.unshift(getRow());
        board[0][0] = 'DURU';
        board[0][1] = 'LR';
        board[0][2] = 'LD';

        return board;
    })();
    Model.StartFalse = (function(){
        var board = clone(Model.StartTrue);
        board[4][2][1] = 1;
        return board;
    })();
    Model.IdDelayed = (function(){
        var board = [];
        for (var i = 0; i < 8; i++){
            board.unshift(builderGetRow(2));
            board.unshift(builderGetRow(2));
            builderCopy(board,0,0,Model._rightDelay);
        }
        return board;
    })();
    /*
        Build a charBoard for the PSPACE reduction
        Params:
            graph: Graph
            input: input vector
            timeExp: exponent for the time (t = 2^timeExp)
        Return:
            board: matrix representing the board, with BlockType.*, [BlockType.*, params...] or {data: [BlockType.*, params...], options.. } as entries
            trains: list of active trains
        Do not use anything else than graph.level from graph
    */
    function boardBuilder(graph, input, timeExp){
        var board = [];
        var getRow = (blockChar) => builderGetRow(boardWidth, blockChar);
        var copy = (r, c, model) => builderCopy(board, r, c, model);
        var compressBoard = function(){ // Remove unnecessary rows from the board
            for (var i = 0; i < board.length; i++){
                var useful = false;
                for (var j = 0; j < board[i].length; j++)
                    if (board[i][j] != "" && board[i][j] != " " && board[i][j] != "DU" && board[i][j] != "UD"){
                        useful = true;
                        break;
                    }
                if (!useful)
                    board.splice(i--, 1);
            }
        }

        var maxLevelNodes = 0, maxOutDeg = 0;
        for (var i = 0; i < graph.levels.length; i++){
            maxLevelNodes = Math.max(maxLevelNodes, graph.levels[i].length);
            for (var j = 0; j < graph.levels[i].length; j++)
                maxOutDeg = Math.max(maxOutDeg, graph.levels[i][j].outEdges.length);
        }
        var nodeWidth = Model.Fanout[0].length+maxOutDeg*2;
        nodeWidth = nodeWidth + nodeWidth % 2;
        var boardWidth = nodeWidth*maxLevelNodes+maxLevelNodes;

        for (var level = 0; level < graph.levels.length; level++){
            // Build nodes
            var nodeHeight = 0;
            var maxLevelOutDeg = 0, sumOutDegree = 0;
            var hasGates = false;
            for (var i = 0; i < graph.levels[level].length; i++){
                var node = graph.levels[level][i];
                switch (node.port){
                    case Port.SRC:
                        nodeHeight = Math.max(nodeHeight, Model.StartTrue.length);
                        break;
                    case Port.AND:
                    case Port.OR:
                        nodeHeight = Math.max(nodeHeight, Model.AND.length);
                        hasGates = true;
                        break;
                    case Port.ID:
                        nodeHeight = Math.max(nodeHeight, 1);
                        break;
                }
                maxLevelOutDeg = Math.max(maxLevelOutDeg, node.outEdges.length);
                sumOutDegree += node.outEdges.length;
            }
            for (var i = 0; i < nodeHeight; i++)
                board.unshift(getRow());
            for (var i = 0; i < graph.levels[level].length; i++){
                var node = graph.levels[level][i];
                var colOffset = i*nodeWidth+1;
                switch (node.port){
                    case Port.SRC:
                        copy(0, colOffset+3, (input[i] ? Model.StartTrue : Model.StartFalse));
                        break;
                    case Port.AND:
                        copy(0, colOffset, Model.AND);
                        break;
                    case Port.OR:
                        copy(0, colOffset, Model.OR);
                        break;
                    case Port.ID:
                        for (var k = 1; k < nodeHeight; k++)
                            board[k][colOffset+2] = "DU";
                        board[0][colOffset+2] = "DR";
                        board[0][colOffset+3] = "LU";
                        if (hasGates)
                            copy(1,colOffset+2,Model.IdDelayed);
                        break;
                }
            }
            if (level < graph.levels.length-1){
                var fanoutArea = (maxLevelOutDeg-1)*(Model.Fanout.length+1)+1,
                        delayArea = sumOutDegree*2+5, 
                        wireArea = sumOutDegree*2+2;
                // Add fanouters
                for (var i = 0; i < fanoutArea; i++)
                    board.unshift(getRow());
                for (var i = 0; i < graph.levels[level].length; i++){
                    var node = graph.levels[level][i];
                    var colOffset = i*nodeWidth;
                    if (node.outEdges.length <= 1){
                        var lineOffset = (maxLevelOutDeg-node.outEdges.length)*(Model.Fanout.length+1);
                        board[lineOffset][colOffset+4] = "DR";
                        for (var k = 5; k < nodeWidth-node.outEdges.length; k++)
                            board[lineOffset][colOffset+k] = "LR";
                        board[lineOffset][colOffset+nodeWidth-1] = "LU";
                    } else {
                        for (var j = 0; j < node.outEdges.length-1; j++){
                            var lineOffset = (maxLevelOutDeg-j-2)*(Model.Fanout.length+1)+1;
                            copy(lineOffset+1, colOffset, Model.Fanout);
                            board[lineOffset][colOffset+6] = "DR";
                            for (var k = 7; k < nodeWidth-j*2-1; k++)
                                board[lineOffset][colOffset+k] = "LR";
                            board[lineOffset][colOffset+nodeWidth-j*2-1] = "LU";
                            if (j < node.outEdges.length-2){
                                board[lineOffset][colOffset+2] = "DR";
                                board[lineOffset][colOffset+3] = "LR";
                                board[lineOffset][colOffset+4] = "LU";
                            }
                        }
                        var lineOffset = (maxLevelOutDeg-node.outEdges.length)*(Model.Fanout.length+1);
                        board[lineOffset+1][colOffset+2] = "DU";
                        board[lineOffset][colOffset+2] = "DR";
                        for (var k = 3; k < nodeWidth-node.outEdges.length*2+1; k++)
                            board[lineOffset][colOffset+k] = "LR";
                        board[lineOffset][colOffset+nodeWidth-node.outEdges.length*2+1] = "LU";
                    }
                    var outEdges = Math.max(1, node.outEdges.length);
                    for (var k = 1; k < outEdges; k++)
                        for (var z = 0; z < (maxLevelOutDeg-outEdges+k-1)*(Model.Fanout.length+1)+1; z++)
                            board[z][colOffset+nodeWidth+2*(k-outEdges)+1] = "DU";
                    for (var z = 0; z < (maxLevelOutDeg-outEdges)*(Model.Fanout.length+1); z++)
                        board[z][colOffset+nodeWidth-outEdges*2+1] = "DU";
                }
                // Wire to next level
                for (var i = 0; i < delayArea+wireArea; i++)
                    board.unshift(getRow(""));
                var wiredPorts = {};
                var baseLine = 1;
                for (var i = 0; i < graph.levels[level].length; i++){
                    var node = graph.levels[level][i];
                    baseLine += node.outEdges.length*2;
                    for (var j = 0; j < node.outEdges.length; j++){
                        var boardBak = board;
                        for (var delayLoop = 0; delayLoop < sumOutDegree; delayLoop++){
                            board = clone(boardBak);
                            var colOffset = (i+1)*nodeWidth - (node.outEdges.length-j)*2+1;
                            for (var k = baseLine; k < wireArea; k++)
                                board[k][colOffset] += "DU";
                            var target = node.outEdges[j].col*nodeWidth + 3;
                            if (wiredPorts[node.outEdges[j].id] !== undefined)
                                target += 2;
                            var deltaH = 2*j+1;
                            for (var k = 0; k < deltaH; k++)
                                board[baseLine-k][colOffset] += "UD";
                            var targetDir = (target > colOffset ? 1 : -1);
                            if (targetDir > 0)
                                board[baseLine-deltaH][colOffset] += "DR";
                            else
                                board[baseLine-deltaH][colOffset] += "DL";
                            for (var k = 1; k < Math.abs(colOffset-target); k++)
                                board[baseLine-deltaH][colOffset+k*targetDir] += "LR";
                            if (targetDir > 0)
                                board[baseLine-deltaH][target] += "LU";
                            else
                                board[baseLine-deltaH][target] += "RU";
                            for (var k = baseLine-deltaH-1; k >= 0; k--)
                                board[k][target] += "DU";
                            // Pre-Delay setup
                            for (var k = 0; k < delayArea; k++)
                                board[wireArea+k][colOffset] += "DU";
                            if (delayLoop > 0){
                                board[wireArea+delayArea-1][colOffset-1] = "UR";
                                board[wireArea+delayArea-1][colOffset] = "DULU";
                                for (var k = 0; k < delayLoop; k++){
                                    board[wireArea+delayArea-2-k][colOffset-1] = "RDUD";
                                    board[wireArea+delayArea-2-k][colOffset] = "DLDU";
                                }
                            }
                            var trains = [];
                            for (var i2 = 0; i2 <= i; i2++)
                                trains.push(new Train(wireArea+delayArea+fanoutArea,nodeWidth*i2+4,'U',Colors.Brown));
                            var tmpBoard = charToBoard(board)
                            var tmpBoard = new Board(tmpBoard.board, trains);
                            var trainTouches = 0;
                            tmpBoard.onTrainTouch = function(a,b){
                                trainTouches++;
                            }
                            // if (i == 1)
                            //     return tmpBoard;
                            tmpBoard.trainTrace();
                            if (trainTouches > 0)
                                continue;
                            board[0][target] = {data: board[0][target], marker: 1};
                            wiredPorts[node.outEdges[j].id] = 1;
                            break;
                        }
                    }
                }
            } else {
                break;
            }
            // Post-Delay
            var trains = [];
            for (var i = 0; i < graph.levels[level].length; i++)
                trains.push(new Train(wireArea+delayArea+fanoutArea,nodeWidth*i+4,'U',Colors.Brown));
            var tmpBoard = charToBoard(board)
            var tmpBoard = new Board(tmpBoard.board, trains, tmpBoard.markers);
            var completionTime = {};
            tmpBoard.onMarkerHit = function(r,c,m){
                completionTime[c] = this.time;
            }
            // if (level == graph.levels.length-1)
                // return tmpBoard;
            tmpBoard.trainTrace();
            // console.log(completionTime);
            var minTime = 1e60, maxTime = 0;
            for (var k in completionTime)
                if (completionTime.hasOwnProperty(k)){
                    minTime = Math.min(minTime, completionTime[k]);
                    maxTime = Math.max(maxTime, completionTime[k]);
                }
            var deltaH = Math.max(0, maxTime-minTime);
            for (var k = 0; k < deltaH; k++)
                board.unshift(getRow());
            for (var k in completionTime)
                if (completionTime.hasOwnProperty(k)){
                    for (var z = 0; z < deltaH; z++)
                        board[z][k] = "DU";
                    for (var z = deltaH-1, delay = maxTime-completionTime[k]; delay > 0; ){
                        var hz = 2, s = 4;
                        while (s <= delay)
                            s = 2*s+(++hz)*2;
                        s = (s-2*(hz--))/2;
                        if (hz > 1){
                            board[z][k] = "UDLU";
                            board[z][k-1] = "UR";
                            for (var h = 1; h < hz; h++){
                                board[z-h][k] = "DLDU";
                                board[z-h][k-1] = "RDDU";
                            }
                            delay -= s;
                            z -= hz;
                        } else {
                            board[z][k] = "DL";
                            board[z][k-1] = "RU";
                            board[z-1][k-1] = "DR";
                            board[z-1][k] = "LU";
                            delay -= 2;
                            z-=2;
                        }
                    }
                }
            // return board;
            // board.unshift(getRow("#"));
        }
        var lastLevel = graph.levels[graph.levels.length-1];
        var deltaH = 1+timeExp+1+1;
        for (var i = 0; i < deltaH; i++)
            board.unshift(getRow());
        for (var i = 0; i < lastLevel.length; i++){
            var colOffset = i*nodeWidth+4;
            board[deltaH-1][colOffset] = 'DU';
            board[deltaH-1][colOffset-1] = 'UL';
            board[deltaH-1][colOffset-2] = 'RU';
            for (var j = 0; j < timeExp; j++){
                board[deltaH-2-j][colOffset] = "DLDU";
                board[deltaH-2-j][colOffset-1] = "DRDU";
                board[deltaH-2-j][colOffset-2] = "DU";
            }
            board[1][colOffset] = (i ? ["P",0] : "DU");
            board[0][colOffset] = ["T",0];
            board[0][colOffset-2] = board[1][colOffset-2] = "DU";
        }
        deltaH = lastLevel.length;
        var deltaH2 = Math.max(6, Math.ceil((lastLevel.length-1)*(nodeWidth*2+6)/(nodeWidth-1)*2))+lastLevel.length;
        var accumulatedDelay = 0;
        for (var i = 0; i < deltaH; i++)
            board.unshift(getRow());
        for (var i = 0; i < deltaH2; i++)
            board.push(getRow());
        for (var i = 0; i < lastLevel.length; i++){
            var colOffset = i*nodeWidth+2;
            var lineOffset = i;
            for (var k = deltaH-1; k > lineOffset; k--)
                board[k][colOffset] = "UD";
            board[lineOffset][colOffset] = "DR";
            for (var k = colOffset+1; k < nodeWidth*maxLevelNodes+(lastLevel.length-i-1); k++)
                board[lineOffset][k] = "LR";
            colOffset = nodeWidth*maxLevelNodes+(lastLevel.length-i-1);
            board[lineOffset][colOffset] = "LD";
            for (var k = lineOffset+1; k < board.length-i; k++)
                board[k][colOffset] = "UD";
            lineOffset = board.length-i-1;
            board[lineOffset][colOffset] = "UL";
            for (var k = colOffset-1; k > i*nodeWidth+4; k--)
                board[lineOffset][k] = "LR";
            colOffset = i*nodeWidth+4;
            board[lineOffset][colOffset] = "RU";
            for (var k = lineOffset-1; k >= board.length-deltaH2; k--)
                board[k][colOffset] = "UD";

            for (var k = lineOffset-1, delay = accumulatedDelay; delay > 0; ){
                if (delay/2 <= nodeWidth-1){
                    var delayWidth = delay/2;
                    board[k][colOffset] = "DL";
                    board[k-1][colOffset] = "LU";
                    for (var z = 1; z < delayWidth; z++)
                        board[k][colOffset-z] = board[k-1][colOffset-z] = "LR";
                    board[k][colOffset-delayWidth] = "RU";
                    board[k-1][colOffset-delayWidth] = "RD";
                    delay -= delayWidth*2;
                    k -= 2
                } else {
                    var hz = 2, s = 4;
                    while (s+2+hz*2 <= delay && hz < nodeWidth-3)
                        s = 2*s+(++hz)*2;
                    s = (s-2*(hz--))/2;
                    if (hz > 1){
                        board[k-2][colOffset] = "LU";
                        board[k-1][colOffset] = " ";
                        board[k][colOffset] = "DL";
                        board[k-2][colOffset-1] = "LR";
                        board[k-1][colOffset-1] = "LD";
                        board[k][colOffset-1] = "LRUL";
                        for (var h = 1; h < hz; h++){
                            board[k-2][colOffset-1-h] = "LR";
                            board[k-1][colOffset-1-h] = "DRRL";
                            board[k][colOffset-1-h] = "RURL";
                        }
                        board[k-2][colOffset-1-hz] = "DR";
                        board[k-1][colOffset-1-hz] = "DU";
                        board[k][colOffset-1-hz] = "RU";
                        delay -= s+2+hz*2;
                        k -= 3;
                    }
                }
                // break;
            }
            // for (var delayRemaining = delay, k = lineOffset-1, delayWidth; delayRemaining > 0; k -= 2, delayRemaining -= 2*delayWidth){
            //     delayWidth = Math.min(delayRemaining/2, nodeWidth-1);
            //     board[k][colOffset] = "DL";
            //     board[k-1][colOffset] = "LU";
            //     for (var z = 1; z < delayWidth; z++)
            //         board[k][colOffset-z] = board[k-1][colOffset-z] = "LR";
            //     board[k][colOffset-delayWidth] = "RU";
            //     board[k-1][colOffset-delayWidth] = "RD";
            // }
            accumulatedDelay += nodeWidth*2+6;
        }
        compressBoard();
        return board;
    }

    return {
        create: function(graph, input, timeExp){
            var charBoard = boardBuilder(graph, input, timeExp);
            // return {board: charBoard, highlightRects: []};
            var blockBoard = charToBoard(charBoard);
            var board = new Board(blockBoard.board);
            return {board: board, highlightRects: blockBoard.highlightRects};
        },
        Model: Model
    };  
})();