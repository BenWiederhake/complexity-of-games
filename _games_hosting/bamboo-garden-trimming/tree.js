class RegularOracle {
    constructor(n) {
        this.n = n;
        this.counter = 0;
        this.next = 0;
    }

    query() {
        var r = this.next;

        this.counter = (this.counter + 1) % (1 << (this.n - 1));
        this.next = 0;
        while ((1 << this.next) & this.counter)
            this.next++;

        return r;
    }
}

class VirtualBamboo {
    constructor(children) {
        this.children = children;
        this.oracle = new RegularOracle(this.children.length)
        this.rate = children.reduce((total, current) => {
            return total + current.rate;
        }, 0);
    }

    query() {
        var i = this.oracle.query();
        return this.children[i].query();
    }
}

class ListNode {
    constructor(rate) {
        this.prev = null;
        this.next = null;
        this.rate = rate;
        this.bucket = [];
    }
}

class Oracle {
    make_list(bamboos) {
        bamboos.sort((x, y) => { return y.rate - x.rate; });
        var head = new ListNode(bamboos[0].rate);
        var current = head;
        for (var i = 0; i < bamboos.length; i++) {
            if (bamboos[i].rate != current.rate) {
                var node = new ListNode(bamboos[i].rate);
                current.next = node;
                node.prev = current;
                current = node;
            }

            current.bucket.push(bamboos[i]);
        }

        var tail = new ListNode(0);
        current.next = tail;
        tail.prev = current;

        return head;
    }

    merge(head) {
        var p2 = head.next;
        for (; p2 != null && p2.rate == p2.prev.rate / 2; p2 = p2.next);

        var p1 = p2;
        for (; p1.prev != null && p1.prev.bucket.length < 2; p1 = p1.prev);

        var C = [];
        var p = head;
        for (var p = head; p != p1; p = p.next)
            C.push(p.bucket.pop());

        if (p1 != head)
            C.push(p.prev.bucket.pop());

        var v = null;
        if (C.length != 0)
            v = new VirtualBamboo(C);

        var R = []
        for (p = p1; p != p2; p = p.next)
            R.push(p.bucket.pop());

        //Remove empty nodes
        for (p = head; p != p2; p = p.next) {
            if (p.bucket.length > 0)
                continue;

            p.next.prev = p.prev;
            if (p.prev != null)
                p.prev.next = p.next;
            else
                head = p.next;
        }

        return [head, v, R];
    }

    normalize(bamboos) {
        var normalized = []

	//Switch to representing fractions. Compute rounded denominators
        var sum = bamboos.reduce((total, current) => total + current.rate, 0);
        for (var i = 0; i < bamboos.length; i++) 
            normalized.push(new Bamboo(bamboos[i],  2**Math.ceil(Math.log2(sum/bamboos[i].rate)) ) );

	//Sort, smallest denominator first
        normalized.sort((x, y) => { return x.rate - y.rate; });
	
	//Switch back to integers
	var target = normalized[normalized.length-1].rate;
        for (var i = 0; i < normalized.length; i++)
		normalized[i].rate = normalized[normalized.length-1].rate / normalized[i].rate;

	//Normalize
	sum = normalized.reduce((total, current) => total + current.rate, 0);
        for (var i = 0; i < normalized.length; i++)
	{
		sum -= normalized[i].rate;
		normalized[i].rate = 2**Math.floor(Math.log2(target-sum));
		sum += normalized[i].rate;
	}

        return normalized;
    }

    constructor(bamboos) {
        var normalized = this.normalize(bamboos);

        while (normalized.length > 1) {
            var head = this.make_list(normalized);
            var next = []
            while (head.rate != 0) {
                var [head, v, R] = this.merge(head);
                if (v != null)
                    next.push(v);

                next.push(...R);
            }

            normalized = next;
        }

        this.root = normalized[0];
    }

    query() {
        return this.root.query().tag;
    }
}





class TreeNode {
    constructor(root, next) {
        if(root instanceof Bamboo)
        {
            this.text = {}
            this.text.name = "b"+(root.tag.tag+1);
            this.text.desc = root.tag.rate + " (" + root.rate + ")";
            this.HTMLclass = "node_real" + (next?" node_next":"");
         }
        else if(root instanceof VirtualBamboo)
        {
            this.text = {}
            this.text.name = root.oracle.counter.toString(2).padStart(root.oracle.n-1, "0");
            this.text.desc = root.rate.toString();
            this.HTMLclass = "node_virtual";
            this.children = []
            for(var i=0; i<root.children.length; i++)
            {
                var pseudo = { pseudo: true, children: [ new TreeNode(root.children[i], next && root.oracle.next==i) ] };
                if(root.oracle.next == i)
                    pseudo.connectors = {style: {'arrow-end': 'block-wide-long' } };
                this.children.push( pseudo );
            } 
        }
    }
}


class TreeDrawer
{
    constructor(oracle, element)
    {
        this.oracle = oracle;

        this.wrapper = document.createElement('div');
        this.div = document.createElement('div');
        this.div.className = "tree"
        this.div.id = "oracle_tree";
        this.wrapper.appendChild(this.div);
        element.appendChild(this.wrapper);
        
        this.chart_config = { chart: { container: "#oracle_tree", connectors: { type: "straight", style: { "stroke-width": 2} }, scrollbar: "native" } };
        this.T = null;
    }
    
    draw()
    {
        this.chart_config.nodeStructure = new TreeNode(this.oracle.root, true);
        
        if(this.T==null)
        { 
            this.T = new Treant(this.chart_config);
            var rect = this.div.getBoundingClientRect();
            this.wrapper.style.width=rect.width.toString() + "px";
            this.wrapper.style.height=rect.height.toString() + "px";
        }
        else
            this.T.tree.reload();
    }
}    
