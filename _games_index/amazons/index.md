---
title: Amazons
short_description: Two players move amazons on a square board. After moving, an amazon shoots an arrow that blocks movement. The last player to move wins.
---

![](amazons.png){:width="250"}

## Description

Two players (Black and White) move amazons on a $n \times n$ board. 
White moves first, and the players alternate moves thereafter. Each move consists of two parts. First, a player moves one of their own amazons one or more empty squares in a straight line (orthogonally or diagonally), exactly as a queen moves in chess; it may not cross or enter a square occupied by an amazon of either color or an arrow. Second, after moving, the amazon shoots an arrow from its landing square to another square, using another queenlike move. This arrow may travel in any orthogonal or diagonal direction (even backwards along the same path the amazon just traveled, into or across the starting square if desired). An arrow, like an amazon, cannot cross or enter a square where another arrow has landed or an amazon of either color stands. The square where the arrow lands is marked to show that it can no longer be used. The last player to be able to make a move wins. Draws are impossible.

An instance consists of $n$ and of the starting configuration of amazons and arrows.

The classical game corresponds to $n=10$, black amazons on a7, d10, g10, j7, white amazons on a4, d1, g1, j4, and no arrows.

## Computational complexity

PSPACE-complete via a reductions from [Hex]({{site.baseurl}}/i/shannon-switching-game-on-vertices) [[1]],  from a variant of Geography [[1]], or from Formula Game [[2]].

Endgames in which pieces of opposing colors are separated from each other are NP-hard [[2]] [[3]].

## References

[[1]] T. Furtak, M. Kiyomi, T. Uno, M. Buro, "Generalized Amazons is PSPACE–Complete", in IJCAI 2005.

[[2]] {% include warning_peer_review.html %} R. A. Hearn, "Amazons is PSPACE-complete", arXiv:cs/0502013, 2005.

[[3]] M. Buro, "Simple Amazons Endgames and Their Connection to Hamilton Circuits in Cubic Subgrid Graphs", in CG 2000.


[Game of the Amazons on Wikipedia](https://en.wikipedia.org/wiki/Game_of_the_Amazons)

[1]: https://www.ijcai.org/Proceedings/05/Papers/1300.pdf 
[2]: https://arxiv.org/pdf/cs/0502013
[3]: https://link.springer.com/chapter/10.1007%2F3-540-45579-5_17 






