# Computational Complexity of Games

## How To Contribute

### Adding a game to the index

1. Fork and clone the repository.

2. Add a subfolder of `_games_index` with the name of the new game. Stick to characters from `a-z`, digits `0-9`, and dashes `-`, e.g., `game-name`.

3. Create an `index.md` file in the newly created subfolder (`_games_index/game-name`). The file should start with the following lines
~~~~
---
title: Human-Readable Game Name
short_description: One-line description of the game.
---
~~~~
The rest of the file follows the markdown syntax. You can add inline LaTeX math to the file by delimiting it with single dollar signs, e.g, `$x^2$`. Display math is delimited by `\[` and `\]`.
To add a picture of the game, place a png file with the game name in the game subfolder. Include the picture in the `index.md` file as follows: `![](game-name.png){:width="250"}`.
For an example game see, e.g., [_games_index/amazons/index.md](_games_index/amazons/index.md).
Links to other games should be prepended by prefix `{{site.baseurl}}`, e.g. `See also [Peg Solitaire]({{site.baseurl}}/i/peg-solitaire/).`.

5. Add yourself to [contributors.md](contributors.md).

6. Test that the result looks good by running `jekyll serve`.

7. Commit, push your changes, and create a pull request.

### Updating a game that is already in the index

1. Fork and clone the repository.

2. Locate the corresponding subfolder of `_games_index`.

3. Edit the files as necessary.

4. Add yourself to [contributors.md](contributors.md).

5. Test that the result looks good by running `jekyll serve`.

6. Commit, push your changes, and create a pull request.

## Hosting Supplementary Material

1. Fork and clone the repository.

2. Add a subfolder of `_games_hosting` with the name of the new game. Stick to characters from `a-z`, digits `0-9`, and dashes `-`, e.g., `game-name`.

3. Add your html files to the `game-name` subfolder.
The files should only include the contents of the `<body>` sections (without the `<body>` and `</body>` tags)
and should start with (replace the title as needed):
~~~~
---
title: My Awesome Game
---
~~~~
If additional javascript scripts or styles are needed in the `head` section of the page, they can be included as follows:
~~~~
---
title: My Awesome Game
custom_css: my_custom_style.css
custom_js: my_custom_script.js
---
~~~~ 
If more than one css/script needs to be included, you can use the following syntax:
~~~~
---
title: My Awesome Game
custom_css:
- my_first_style.css
- my_second_style.css
custom_js:
- script_01.js
- script_02.js
- script_03.js
---
~~~~

The pages that should appear in the list of hosted content must also include an additional short description, e.g.:
~~~~
---
title: My Awesome Game
short_description: One line description of my awesome interactive content.
---
~~~~

4. Add yourself to [contributors.md](contributors.md).

5. Test that the result looks good by running `jekyll serve`.

6. Commit, push your changes, and create a pull request.


